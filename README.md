Directory Layout
==============

* `bin/ ` — where every building and conversion must go
* `src/ ` — where source code and original data are stored
	* `core/` — a logic of the application. Each subsystem must have its own sudirectory
		* `init/` — initialization subsystem
	* `ui/` — user interface related files
		* `gui/` — Graphical User Interface subset related files
			* `template/` — GUI generalized tempolates
