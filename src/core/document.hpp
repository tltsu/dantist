/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once


/* INCLUDES *******************************/

#include <QString>
#include <QVector3D>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <QtCore/qstringlist.h>


/* DECLARATIONS ***************************/

struct Document
{
	Document();

	QStringList path;

	vtkRenderer* scene() const;

	void setAxisRotation(qreal x, qreal y, qreal z);

private:
	static vtkSmartPointer<vtkActor> createAxis();


	vtkSmartPointer<vtkRenderer> _scene;
	vtkSmartPointer<vtkActor> _axisActor;

	QVector3D _lightPosition;
};
