/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* INCLUDES *******************************/

#include <QVector3D>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkLight.h>
#include <vtkLightActor.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkTransform.h>
#include <vtkLine.h>
#include <array>
#include "document.hpp"


/* PUBLIC FUNCTIONS ***********************/

Document::Document()
	: _lightPosition(0.0, 0.0, 0.0)
	, _scene(vtkSmartPointer<vtkRenderer>::New())
	, _axisActor(createAxis())
{
	_scene->GradientBackgroundOn();
	_scene->SetBackground(0.502, 0.651, 1.0);
//	_scene->SetBackground2(0.349, 0.561, 0.741);


	auto light = vtkSmartPointer<vtkLight>::New();
	light->SetLightTypeToSceneLight();
	light->SetPosition(_lightPosition.x(), _lightPosition.y(), _lightPosition.z());
	light->SetPositional(true);
	light->SetConeAngle(10.0);
	light->SetFocalPoint(0.0, 0.0, 0.0);
	light->SetDiffuseColor(1.0, 1.0, 1.0);
	light->SetAmbientColor(0.2, 0.2, 0.2);

	auto lightActor = vtkSmartPointer<vtkLightActor>::New();
	lightActor->SetLight(light);
	_scene->AddViewProp(lightActor);
}

void Document::setAxisRotation(qreal x, qreal y, qreal z)
{
	auto transform = vtkSmartPointer<vtkTransform>::New();
	transform->PostMultiply();
	transform->RotateX(x);
	transform->RotateY(y);
	transform->RotateZ(z);

	_axisActor->SetUserTransform(transform);
}

vtkRenderer* Document::scene() const
{
	return _scene.Get();
}


/* PRIVATE FUNCTIONS **********************/

vtkSmartPointer<vtkActor> Document::createAxis()
{
	auto linesPolyData = vtkSmartPointer<vtkPolyData>::New();

	static const std::array<QVector3D, 4> points =
		{{
			QVector3D(0.0, 0.0, 0.0),
			QVector3D(1.0, 0.0, 0.0),
			QVector3D(0.0, 1.0, 0.0),
			QVector3D(0.0, 0.0, 1.0),
		}};
	auto pts = vtkSmartPointer<vtkPoints>::New();
	for(const QVector3D& point : points)
		pts->InsertNextPoint(point.x(), point.y(), point.z());

    linesPolyData->SetPoints(pts);


    std::array<vtkSmartPointer<vtkLine>, 3> segments =
    	{{
    		vtkSmartPointer<vtkLine>::New(),
    		vtkSmartPointer<vtkLine>::New(),
    		vtkSmartPointer<vtkLine>::New()
		}};

    for(size_t i = 0; i < segments.size(); ++i)
    {
    	segments[i]->GetPointIds()->SetId(0, 0);
    	segments[i]->GetPointIds()->SetId(1, i + 1);
    }


	auto lines = vtkSmartPointer<vtkCellArray>::New();
	for(vtkSmartPointer<vtkLine> segment : segments)
		lines->InsertNextCell(segment);


	static const std::array<std::array<uchar, 3>, 3> colorValues =
		{{
			{{255, 0,   0  }},
			{{0,   255, 0  }},
			{{0,   0,   255}},
		}};

	auto colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
	colors->SetNumberOfComponents(3);
	for(const std::array<uchar, 3>& color : colorValues)
		colors->InsertNextTupleValue(color.data());

	linesPolyData->GetCellData()->SetScalars(colors);


	auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(linesPolyData);


	auto actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	return actor;
}
