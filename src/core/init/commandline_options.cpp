/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* INCLUDES *******************************/

#include "commandline_options.hpp"


/* PUBLIC FUNCTIONS ***********************/

Utils::CommandlineOptions::CommandlineOptions(QStringList arguments)
{
	_parser.addHelpOption();
	_parser.addVersionOption();

	_parser.addPositionalArgument(QStringLiteral("file"), tr("File to open on startup"));

	_parser.parse(arguments);
}

QStringList Utils::CommandlineOptions::filePath() const
{
	return _parser.positionalArguments();
}
