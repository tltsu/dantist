/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once


/* INCLUDES *******************************/

#include <QCommandLineParser>
#include <QCoreApplication>


/* DECLARATIONS ***************************/

namespace Utils
{
	class CommandlineOptions
	{
		Q_DECLARE_TR_FUNCTIONS(CommandlineOptions)

	public:
		/** Exists the application if help option is provided */
		CommandlineOptions(QStringList arguments);

		QStringList filePath() const;

	private:
		QCommandLineParser _parser;
	};
}
