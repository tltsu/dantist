/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* INCLUDES *******************************/

#include "main_window.hpp"
#include "KeyPressInteractorStyle.h"
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkSTLReader.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkObjectFactory.h>
#include <vtkEventQtSlotConnect.h>
#include <QGridLayout>
#include <QHBoxLayout>
#include <vtkActor.h>
#include <vtkAxesActor.h>
#include <vtkCamera.h>
#include <vtkTransform.h>

#ifdef Q_OS_OSX
#include "osxHelper.h"
#endif

#define _USE_MATH_DEFINES
#include <math.h>


vtkStandardNewMacro(KeyPressInteractorStyle);

/* PUBLIC FUNCTIONS ***********************/

GUI::MainWindow::MainWindow(QString openedFilePath)
	: MainWindowTemplate(openedFilePath)
	, _axisRotation(QStringLiteral("Ось"))
	, _axisXRotation(Qt::Vertical)
	, _axisXRotationLabel(QStringLiteral("X"))
	, _axisYRotation(Qt::Vertical)
	, _axisYRotationLabel(QStringLiteral("Y"))
	, _axisZRotation(Qt::Vertical)
	, _axisZRotationLabel(QStringLiteral("Z"))
{
	setSupportedFileFormats(QStringLiteral("3D models (*.stl)"));
	initComponents();
	initLayout();
}


/* PROTECTED FUNCTIONS ********************/

QString GUI::MainWindow::documentPath() const
{
	return _document.path.at(0);
}

void GUI::MainWindow::createDocument()
{
}

bool GUI::MainWindow::openDocument(QStringList path)
{

	std::cout << "open \n" << std::endl;
	_document = Document();
	_document.path = path;

	_viewport.GetRenderWindow()->AddRenderer(_document.scene());

	auto renderWindowInteractor =
			vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWindowInteractor->SetRenderWindow(_viewport.GetRenderWindow());
	auto style =
			vtkSmartPointer<KeyPressInteractorStyle>::New();

	style->SetCurrentRenderer(_document.scene());

	if (!path.at(0).isNull()) {
		auto readerUp = vtkSmartPointer<vtkSTLReader>::New();
		readerUp->SetFileName(path.at(0).toLatin1().data());
		readerUp->Update();

		auto connectivityFilterUp =
				vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
		connectivityFilterUp->SetInputData(readerUp->GetOutput());
		connectivityFilterUp->SetExtractionModeToLargestRegion();
		connectivityFilterUp->Update();

		auto modelUpMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		modelUpMapper->SetInputConnection(connectivityFilterUp->GetOutputPort());

		auto modelUp = vtkSmartPointer<vtkActor>::New();
		modelUp->GetProperty()->SetColor(0.6, 0.6, 0.6);
		modelUp->SetMapper(modelUpMapper);

		_document.scene()->AddActor(modelUp);
		style->pdUp = connectivityFilterUp->GetOutput();
	}

	if (path.count()>1) {

		if (!path.at(1).isNull()) {

			auto readerLo = vtkSmartPointer<vtkSTLReader>::New();
			readerLo->SetFileName(path.at(1).toLatin1().data());
			readerLo->Update();

			auto connectivityFilterLo =
					vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
			connectivityFilterLo->SetInputData(readerLo->GetOutput());
			connectivityFilterLo->SetExtractionModeToLargestRegion();
			connectivityFilterLo->Update();

			auto modelLoMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
			modelLoMapper->SetInputConnection(connectivityFilterLo->GetOutputPort());

			auto modelLo = vtkSmartPointer<vtkActor>::New();
			modelLo->GetProperty()->SetColor(0.6, 0.6, 0.6);
			modelLo->SetMapper(modelLoMapper);
			_document.scene()->AddActor(modelLo);
			style->pdLo = connectivityFilterLo->GetOutput();

		}
	}
	renderWindowInteractor->SetInteractorStyle(style);
	auto iren = vtkSmartPointer<QVTKInteractor>::New();
	_viewport.GetRenderWindow()->SetInteractor(iren);
	iren->Initialize();
	iren->SetInteractorStyle(style);

	QMenu* popup1 = new QMenu(&_viewport);
	popup1->addAction("Select");
	popup1->addAction("Cut");
	connect(popup1, SIGNAL(triggered(QAction*)), this, SLOT(editMenu(QAction *)));

	vtkEventQtSlotConnect* Connections = vtkEventQtSlotConnect::New();

	Connections->Connect(_viewport.GetRenderWindow()->GetInteractor(),
						 vtkCommand::RightButtonPressEvent,
						 this,
						 SLOT(popup( vtkObject*, unsigned long, void*,
								 void*, vtkCommand*)),
						 popup1, 1.0);

	auto axes = vtkSmartPointer<vtkAxesActor>::New();
	//_viewport.GetRenderWindow()->SetOrientationMarker(axes);
	_viewport.GetRenderWindow()->AddRenderer(_document.scene());

	axisRotationChanged();

	return true;
}

void GUI::MainWindow::popup(vtkObject * obj, unsigned long, void * client_data,
					   void *, vtkCommand * command)
{
	// A note about context menus in Qt and the QVTKWidget
	// You may find it easy to just do context menus on right button up,
	// due to the event proxy mechanism in place.

	// That usually works, except in some cases.
	// One case is where you capture context menu events that
	// child windows don't process.  You could end up with a second
	// context menu after the first one.

	// get interactor
	vtkRenderWindowInteractor* iren =
			vtkRenderWindowInteractor::SafeDownCast(
					obj);
	// consume event so the interactor style doesn't get it
	command->AbortFlagOn();
	// get popup menu
	QMenu* popupMenu = static_cast<QMenu*> (client_data);
	// get event location
	int* sz = iren->GetSize();
	int* position = iren->GetEventPosition();
	// remember to flip y
	QPoint pt = QPoint(position[0], sz[1] - position[1]);
	// map to global
	QPoint global_pt = popupMenu->parentWidget()->mapToGlobal(pt);
	// show popup menu at global point
	popupMenu->popup(global_pt);
}






bool GUI::MainWindow::saveDocument(QString path)
{
	Q_UNUSED(path)

	return true;
}


/* PRIVATE SLOTS **************************/

void GUI::MainWindow::axisRotationChanged()
{
	const qreal x = /*(*/_axisXRotation.value() /*/ 360.) * (M_PI * 2.0)*/;
	const qreal y = /*(*/_axisYRotation.value() /*/ 360.) * (M_PI * 2.0)*/;
	const qreal z = /*(*/_axisZRotation.value() /*/ 360.) * (M_PI * 2.0)*/;

	_document.setAxisRotation(x, y, z);

	vtkCamera* camera = _document.scene()->GetActiveCamera();

	auto transform = vtkSmartPointer<vtkTransform>::New();
	transform->PostMultiply();
	transform->RotateX(x);
	transform->RotateY(y);
	transform->RotateZ(z);

	camera->SetUserTransform(transform);
	_document.scene()->SetActiveCamera(camera);
}

void GUI::MainWindow::useAxis(bool use)
{

}


/* PRIVATE FUNCTIONS **********************/

void GUI::MainWindow::initComponents()
{
	// Internal ones (from children to parents)
#ifdef Q_OS_OSX
	disableGLHiDPI(_viewport.winId());
#endif
    _axisXRotation.setRange(0, 360);
	_axisXRotation.setPageStep(10);
	_axisXRotation.setTickInterval(10);
	connect(
		&_axisXRotation, &QSlider::valueChanged,
		this, &MainWindow::axisRotationChanged
	);

	_axisYRotation.setRange(0, 360);
	_axisYRotation.setPageStep(10);
	_axisYRotation.setTickInterval(10);
	connect(
		&_axisXRotation, &QSlider::valueChanged,
		this, &MainWindow::axisRotationChanged
	);

	_axisZRotation.setRange(0, 360);
	_axisZRotation.setPageStep(10);
	_axisZRotation.setTickInterval(10);
	connect(
		&_axisZRotation, &QSlider::valueChanged,
		this, &MainWindow::axisRotationChanged
	);


	_axisXRotationLabel.setBuddy(&_axisXRotation);
	_axisYRotationLabel.setBuddy(&_axisYRotation);
	_axisZRotationLabel.setBuddy(&_axisZRotation);


	_axisRotation.setCheckable(true);
	connect(
		&_axisRotation, &QGroupBox::clicked,
		this, &MainWindow::useAxis
	);
	_viewport.GetRenderWindow()->AddRenderer(_document.scene());

}

// The layout must be filled from children to parents
void GUI::MainWindow::initLayout()
{
	QGridLayout* const rotationGroupLayout = new QGridLayout(&_axisRotation);
	rotationGroupLayout->addWidget(&_axisXRotationLabel, 0, 0);
	rotationGroupLayout->addWidget(&_axisYRotationLabel, 0, 1);
	rotationGroupLayout->addWidget(&_axisZRotationLabel, 0, 2);
	rotationGroupLayout->addWidget(&_axisXRotation, 1, 0);
	rotationGroupLayout->addWidget(&_axisYRotation, 1, 1);
	rotationGroupLayout->addWidget(&_axisZRotation, 1, 2);


	QHBoxLayout* const mainWindowContentLayout = new QHBoxLayout(&_mainWindowContent);
	mainWindowContentLayout->addWidget(&_viewport);
	mainWindowContentLayout->addWidget(&_axisRotation);

	setCentralWidget(&_viewport);


}


void GUI::MainWindow::editMenu(QAction *action) {

	if(action->text() == "Select") {
		this->selectAction();
	}
	else if(action->text() == "Cut") {
		this->cutAction();
	}
	_viewport.update();

}

QAction &GUI::MainWindow::cutAction() {
	auto style =
	static_cast<KeyPressInteractorStyle *>(_viewport.GetInteractor()->GetInteractorStyle());
	if (style->start())
		style->extract();
	return MainWindowTemplate::cutAction();
}


QAction &GUI::MainWindow::selectAction() {
	auto style =
			static_cast<KeyPressInteractorStyle *>(_viewport.GetInteractor()->GetInteractorStyle());
	if (style->start())
		style->distinguish();
	return MainWindowTemplate::selectAction();
}

