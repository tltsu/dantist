//
// Created by Ирина Пупыкина on 23.11.15.
//

#include "osxHelper.h"

#include <Cocoa/Cocoa.h>
#include "osxHelper.h"

void disableGLHiDPI( long a_id ){
    NSView* view = reinterpret_cast<NSView*>( a_id );
[view setWantsBestResolutionOpenGLSurface:NO];
}