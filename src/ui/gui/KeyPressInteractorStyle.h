//
// Created by pupykina on 10.11.15.
//

#ifndef DANTIST_KEYPRESSINTERACTORSTYLE_H
#define DANTIST_KEYPRESSINTERACTORSTYLE_H


#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkActor.h>
#include <vtkOrientedGlyphContourRepresentation.h>
#include <vtkContourWidget.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkCommand.h>

class KeyPressInteractorStyle : public vtkInteractorStyleSwitch {
public:

    static KeyPressInteractorStyle* New();
    vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleSwitch);
    virtual void OnChar();

    void distinguish ();
    void extract ();
    void view ();
    void keep ();
    void bounds ();

    bool start ();

    vtkSmartPointer<vtkPolyData> pdUp;
    vtkSmartPointer<vtkPolyData> pdLo;
    KeyPressInteractorStyle(){
        rep = vtkSmartPointer <vtkOrientedGlyphContourRepresentation>::New();
        actorCollection =
                vtkSmartPointer<vtkActorCollection>::New();
        this->SetCurrentStyleToTrackballCamera();
        filename = "/home/pupykina/ORTODONT/ReadSTL/test.wrl";

    };
    vtkSmartPointer<vtkRenderWindowInteractor> rwi;
    vtkSmartPointer<vtkRenderWindow> renderWindow;
    vtkSmartPointer<vtkRenderer> renderer;
    vtkSmartPointer<vtkActor> actorUp;
    vtkSmartPointer<vtkActor> actorLo;
    vtkSmartPointer<vtkOrientedGlyphContourRepresentation> rep;
    vtkSmartPointer<vtkContourWidget> contourWidget;
    vtkSmartPointer<vtkActorCollection> actorCollection;

    std::string filename;
};


#endif //DANTIST_KEYPRESSINTERACTORSTYLE_H
