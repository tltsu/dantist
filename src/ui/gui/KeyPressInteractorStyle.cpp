//
// Created by pupykina on 10.11.15.
//

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleSwitch.h>
#include "KeyPressInteractorStyle.h"
#include <vtkSTLReader.h>
#include <vtkPolyDataMapper.h>
#include "vtkPolyDataCollection.h"
#include "vtkPolyDataWriter.h"
#include <vtkProperty.h>
#include <vtkClipPolyData.h>
#include "vtkPolygonalSurfacePointPlacer.h"
#include "vtkPolygonalSurfaceContourLineInterpolator.h"
#include <vtkTransform.h>
#include <vtkSelectPolyData.h>
#include "vtkEvent.h"
#include "vtkCallbackCommand.h"
#include <vtkVRMLExporter.h>
#include <vtkVRMLImporter.h>
#include <vtkFeatureEdges.h>
#include <vtkFillHolesFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkSelectionNode.h>
#include <vtkInformation.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkCamera.h>
#include <vtkExtractSelection.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkSphereSource.h>
#include <vtkAppendPolyData.h>
#include <vtkCellPicker.h>
#include <vtkPlane.h>
#include <vtkCellArray.h>
#include <vtkMath.h>
#include <vtkContourFilter.h>
#include <vtkRegularPolygonSource.h>
#include <vtkPolyLine.h>
#include <vtkScalarBarActor.h>
#include <vtkPolyDataSilhouette.h>
#include <vtkCutter.h>
#include <vtkRuledSurfaceFilter.h>
#include <vtkWarpScalar.h>
#include <vtkStripper.h>


bool KeyPressInteractorStyle::start() {
    rwi = this->Interactor;
    renderWindow = rwi->GetRenderWindow();
    renderer = this->GetCurrentRenderer();
    actorUp = (vtkActor*) renderer->GetActors()->GetItemAsObject(0);
    actorLo = (vtkActor*) renderer->GetActors()->GetItemAsObject(1);
    if (actorUp!=nullptr){
        return true;
    } else return false;
}

void KeyPressInteractorStyle::OnChar() {

    if (start()) {

        // Output the key that was pressed
        // Get the keypress
        std::string key = rwi->GetKeySym();


        std::cout << "Pressed " << this->Interactor->GetKeyCode() << std::endl;
        switch (this->Interactor->GetKeyCode()) {
            case 'j':
            case 'J': {
                this->JoystickOrTrackball = VTKIS_JOYSTICK;
                this->EventCallbackCommand->SetAbortFlag(1);
                break;
            }
            case 't':
            case 'T': {
                this->JoystickOrTrackball = VTKIS_TRACKBALL;
                this->EventCallbackCommand->SetAbortFlag(1);
                break;
            }
            case 'c':
            case 'C': {
                this->CameraOrActor = VTKIS_CAMERA;
                this->EventCallbackCommand->SetAbortFlag(1);
                break;
            }
            case 'a':
            case 'A': {
                this->CameraOrActor = VTKIS_ACTOR;
                this->EventCallbackCommand->SetAbortFlag(1);
                break;
            }

            case 'd':
            case 'D': {
                std::cout << "The _d_istinguish key was pressed." << std::endl;
                this->distinguish();
                break;
            }

            case 'x':
            case 'X': {
                std::cout << "The e_x_tract key was pressed." << std::endl;
                this->extract();
                break;

            }

            case 'b':
            case 'B': {
                std::cout << "The _b_ounds key was pressed." << std::endl;
                this->bounds();
                break;

            }

            case 'u':
            case 'U': {
                std::cout << "The _u_p key was pressed. " << std::endl;

                if (actorUp) {

                    if (actorUp->GetVisibility()) actorUp->VisibilityOff(); else actorUp->VisibilityOn();
                    renderer->ResetCamera();
                    renderWindow->Render();
                }
                break;

            }

            case 'l':
            case 'L': {
                std::cout << "The _l_ow key was pressed. " << std::endl;
                if (actorLo) {
                    if (actorLo->GetVisibility()) actorLo->VisibilityOff(); else actorLo->VisibilityOn();
                    renderer->ResetCamera();
                    renderWindow->Render();
                }
                break;

            }

            case 'k':
            case 'K': {
                std::cout << "The k_eep key was pressed." << std::endl;
                this->keep();
                std::cout << "save" << std::endl;
                break;
            }

            case 'v':
            case 'V': {
                std::cout << "The v_iew key was pressed." << std::endl;
                // VRML Import
                this->view();
                std::cout << "open" << std::endl;
                break;
            }

        }
    }
// Set the CurrentStyle pointer to the picked style
        this->SetCurrentStyle();

};


void KeyPressInteractorStyle::distinguish (){

    vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actorUp->GetMapper()->GetInputAsDataSet());

    vtkSmartPointer<vtkTransform> transform =
            vtkSmartPointer<vtkTransform>::New();
    transform->SetMatrix(actorUp->GetMatrix());

    vtkSmartPointer<vtkTransformPolyDataFilter> transformPolyData =
            vtkSmartPointer<vtkTransformPolyDataFilter>::New();

    transformPolyData->SetInputData(polydata);
    transformPolyData->SetTransform(transform);
    transformPolyData->Update();

    pdUp = transformPolyData->GetOutput();
    //FIXME update actor without actor create
    vtkSmartPointer<vtkActor> actorN;
    actorN = vtkActor :: New();
    vtkSmartPointer<vtkPolyDataMapper> mapper2 =vtkPolyDataMapper::New();
    mapper2->SetInputData(pdUp);
    actorN->SetMapper(mapper2);

    actorUp=actorN;
    actorUp->GetProperty()->SetInterpolationToFlat();
    contourWidget = vtkSmartPointer<vtkContourWidget>::New();
    contourWidget->Modified();
    contourWidget->SetInteractor(rwi);

    rep=
            vtkOrientedGlyphContourRepresentation::SafeDownCast(
                    contourWidget->GetRepresentation());
    rep->GetLinesProperty()->SetColor(1, 0.2, 0);
    rep->GetLinesProperty()->SetLineWidth(3.0);
    vtkSmartPointer<vtkPolygonalSurfacePointPlacer> pointPlacer =
            vtkSmartPointer<vtkPolygonalSurfacePointPlacer>::New();
    pointPlacer->AddProp(actorUp);

    pointPlacer->GetPolys()->AddItem( pdUp);
    rep->SetPointPlacer(pointPlacer);
    vtkSmartPointer<vtkPolygonalSurfaceContourLineInterpolator> interpolator =
            vtkSmartPointer<vtkPolygonalSurfaceContourLineInterpolator>::New();
    interpolator->GetPolys()->AddItem( pdUp );
    rep->SetLineInterpolator(interpolator);

    contourWidget->EnabledOn();
    if (actorLo) {
        renderer->GetActors()->ReplaceItem(1, actorLo);
    }
    if (actorUp) {
        renderer->GetActors()->ReplaceItem(0, actorUp);
    }
}


void KeyPressInteractorStyle::view () {
    vtkSmartPointer<KeyPressInteractorStyle> style =
            vtkSmartPointer<KeyPressInteractorStyle>::New();
    vtkSmartPointer<vtkRenderer> rendererOpen =
            vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> renderOpenWindow =
            vtkSmartPointer<vtkRenderWindow>::New();
    rendererOpen->SetBackground(.1, .1, .1);
    renderOpenWindow->AddRenderer(rendererOpen);
    vtkSmartPointer<vtkRenderWindowInteractor> renderOpenWindowInteractor =
            vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderOpenWindowInteractor->SetInteractorStyle(style);
    renderOpenWindowInteractor->SetRenderWindow(renderOpenWindow);

    vtkSmartPointer<vtkVRMLImporter> importer = vtkSmartPointer<vtkVRMLImporter>::New();
    importer->SetFileName ( filename.c_str() );
    importer->Read();
    importer->SetRenderWindow(renderOpenWindow);
    importer->Update();

    renderOpenWindow->Render();
    renderOpenWindowInteractor->Start();
}

void KeyPressInteractorStyle::bounds () {
    vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actorUp->GetMapper()->GetInputAsDataSet());

    vtkSmartPointer<vtkTransform> transform =
            vtkSmartPointer<vtkTransform>::New();
    transform->SetMatrix(actorUp->GetMatrix());

    vtkSmartPointer<vtkTransformPolyDataFilter> transformPolyData =
            vtkSmartPointer<vtkTransformPolyDataFilter>::New();

    transformPolyData->SetInputData(polydata);
    transformPolyData->SetTransform(transform);
    transformPolyData->Update();

    pdUp = transformPolyData->GetOutput();
    //FIXME update actor without actor create
    vtkSmartPointer<vtkActor> actorN;
    actorN = vtkActor :: New();
    vtkSmartPointer<vtkPolyDataMapper> mapper2 =vtkPolyDataMapper::New();
    mapper2->SetInputData(pdUp);
    actorN->SetMapper(mapper2);

    actorUp=actorN;
    actorUp->GetProperty()->SetInterpolationToFlat();



//
//
//
//    vtkSmartPointer<vtkContourFilter>  iso = vtkSmartPointer<vtkContourFilter>::New();
//    iso->SetInputData(pd);
////    iso->SetInput((vtkDataSet *) mapper2->GetOutput());
//    iso->SetValue(0, 5);
//    iso->ComputeGradientsOn();
//    iso->Update();
//
//    vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
//    normals->SetInputData(iso->GetOutput());
//    normals->SetFeatureAngle(90.0);
//    normals->ComputeCellNormalsOn();
//    normals->ComputePointNormalsOn();
//    normals->Update();
//
////  using normals as vectors ??
//    vtkSmartPointer<vtkPointData> pds=(normals->GetOutput())->GetPointData();
//    vtkSmartPointer<vtkCellData> cd= (normals->GetOutput())->GetCellData();
//    cd->SetVectors(cd->GetNormals());
//    pds->SetVectors(pds->GetNormals());
//
//    vtkSmartPointer<vtkVectorNorm> gradient = vtkSmartPointer<vtkVectorNorm>::New();
//    gradient->SetInputData(normals->GetOutput());
//    gradient->SetAttributeModeToUseCellData();
//    //gradient->NormalizeOn();
//
//    vtkSmartPointer<vtkPolyDataMapper> skinMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
//    skinMapper->SetInputData(gradient->GetPolyDataOutput());
//    skinMapper->ScalarVisibilityOn();
//    skinMapper->SetScalarRange (1, 5);
//    //((gradient-> GetOutput())->GetScalarRange());
//    skinMapper->SetColorModeToMapScalars();
//    double* arr = (gradient-> GetOutput())->GetScalarRange();
//
//    cout << arr[0] << " " << arr[1] << endl;
//
////	skinMapper -> SetScalarModeToUsePointData();
//
//
//    vtkSmartPointer<vtkActor> skin = vtkSmartPointer<vtkActor>::New();
//    skin->SetMapper(skinMapper);

    //Compute the silhouette
    vtkSmartPointer<vtkPolyDataSilhouette> silhouette =
            vtkSmartPointer<vtkPolyDataSilhouette>::New();
#if VTK_MAJOR_VERSION <= 5
  silhouette->SetInput(polyData);
#else
    silhouette->SetInputData(pdUp);
#endif
    silhouette->SetCamera(renderer->GetActiveCamera());
    //silhouette->BorderEdgesOff();
    silhouette->SetEnableFeatureAngle(1);
    silhouette->SetFeatureAngle(70);

    //create mapper and actor for silouette
    vtkSmartPointer<vtkPolyDataMapper> mapper3 =
            vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper3->SetInputConnection(silhouette->GetOutputPort());

    vtkSmartPointer<vtkActor> actor2 =
            vtkSmartPointer<vtkActor>::New();
    actor2->SetMapper(mapper3);
    actor2->GetProperty()->SetColor(1.0, 0.3882, 0.2784); // tomato
    actor2->GetProperty()->SetLineWidth(5);


    renderer->RemoveAllViewProps();

    renderer->AddActor(actor2);
    renderer->AddActor(actorUp);

    renderWindow->Modified();
    renderWindow->Render();
    rwi->Start();
}


void KeyPressInteractorStyle::keep () {
    vtkSmartPointer<vtkVRMLExporter> vrml = vtkSmartPointer<vtkVRMLExporter>::New();
    vrml->SetFileName(filename.c_str() );
    vrml->SetInput(renderWindow);
    vrml->Write();
}


void KeyPressInteractorStyle::extract () {

    std::cout << "extract start" << std::endl;
    double normalTooth [] = {.0, .0, 1.0};
    int axes = normalTooth[0]*0+normalTooth[1]*1+normalTooth[2]*2;

    vtkSmartPointer<vtkPolyData> contours = rep->GetContourRepresentationAsPolyData();
    double* center = contours->GetCenter();

    contourWidget->EnabledOff();
    vtkSmartPointer<vtkSelectPolyData> loop =
            vtkSmartPointer<vtkSelectPolyData>::New();
    loop->SetInputData(pdUp);
    loop->SetLoop(contours->GetPoints());
    loop->GenerateSelectionScalarsOn();
    loop->InsideOutOn();
    loop->GenerateUnselectedOutputOff();
    loop->SetClosestPoint(center);
    loop->SetSelectionModeToClosestPointRegion();

//    loop->SetSelectionModeToLargestRegion(); // scalars positive inside
    loop->Update();

    vtkSmartPointer<vtkClipPolyData> clip = //clips out negative  region
            vtkSmartPointer<vtkClipPolyData>::New();

    clip->SetInputConnection(loop->GetOutputPort());
    clip->Update();

    vtkSmartPointer<vtkSelectPolyData> loopAll =
            vtkSmartPointer<vtkSelectPolyData>::New();
    loopAll->SetInputData(pdUp);
    loopAll->SetLoop(contours->GetPoints());
    loopAll->GenerateSelectionScalarsOn();
    loopAll->InsideOutOn();
    loopAll->GenerateUnselectedOutputOff();
    loopAll->Update();

    vtkSmartPointer<vtkClipPolyData> clipAll = //clips out positive  region
            vtkSmartPointer<vtkClipPolyData>::New();

    clipAll->SetInputConnection(loopAll->GetOutputPort());
    clipAll->Update();

//    // close surface all
//    vtkSmartPointer<vtkFillHolesFilter> allFillHolesFilter =
//            vtkSmartPointer<vtkFillHolesFilter>::New();
//
//    allFillHolesFilter->SetInputData(clipAll->GetOutput());
//    allFillHolesFilter->SetHoleSize(10000.0);
//    allFillHolesFilter->Update();
//
//    // Make the triangle windong order consistent
//    vtkSmartPointer<vtkPolyDataNormals> normalsAll =
//            vtkSmartPointer<vtkPolyDataNormals>::New();
//    normalsAll->SetInputConnection(allFillHolesFilter->GetOutputPort());
//    normalsAll->ConsistencyOn();
//    normalsAll->SplittingOff();
//    normalsAll->Update();
//
//    // Restore the original normals
//    normalsAll->GetOutput()->GetPointData()->SetNormals(clipAll->GetOutput()->GetPointData()->GetNormals());
//    normalsAll->Update();
//    pdUp=normalsAll->GetOutput();
//    // closed
    pdUp=clipAll->GetOutput();

    // Now extract feature edges
    vtkSmartPointer<vtkFeatureEdges> boundaryEdges =
            vtkSmartPointer<vtkFeatureEdges>::New();

    boundaryEdges->SetInputData(clip->GetOutput());

    boundaryEdges->BoundaryEdgesOn();
    boundaryEdges->FeatureEdgesOff();
    boundaryEdges->NonManifoldEdgesOff();
    boundaryEdges->ManifoldEdgesOff();

    vtkSmartPointer<vtkStripper> boundaryStrips =
            vtkSmartPointer<vtkStripper>::New();
    boundaryStrips->SetInputConnection(boundaryEdges->GetOutputPort());
    boundaryStrips->Update();

    // Change the polylines into polygons
    vtkSmartPointer<vtkPolyData> boundaryPoly =
            vtkSmartPointer<vtkPolyData>::New();
    boundaryPoly->SetPoints(boundaryStrips->GetOutput()->GetPoints());
    boundaryPoly->SetPolys(boundaryStrips->GetOutput()->GetLines());
    boundaryPoly->SetLines(boundaryStrips->GetOutput()->GetLines());

    vtkSmartPointer<vtkPolyData> pdE = vtkSmartPointer<vtkPolyData>::New();
    pdE->SetPolys(boundaryPoly->GetLines());
    pdE->SetPoints(boundaryPoly->GetPoints());
    pdE->SetLines(boundaryPoly->GetLines());


    vtkSmartPointer<vtkPolyData> pdE2 = vtkSmartPointer<vtkPolyData>::New();
    pdE2->SetPolys(boundaryPoly->GetLines());
    pdE2->SetPoints(boundaryPoly->GetPoints());
    pdE2->SetLines(boundaryPoly->GetLines());

    double n [] = {.0, .0, 1.0};
    vtkSmartPointer<vtkTransform> translation =
            vtkSmartPointer<vtkTransform>::New();
    translation->Translate(normalTooth);

    vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter =
            vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    transformFilter->SetInputData(pdE2);
    transformFilter->SetTransform(translation);
    transformFilter->Update();

    double* centerTr = transformFilter->GetOutput()->GetCenter();

    vtkSmartPointer<vtkTransform> shrinkTranslation =
            vtkSmartPointer<vtkTransform>::New();
    shrinkTranslation->Translate(centerTr[0], centerTr[1], centerTr[2]);
    shrinkTranslation->Scale(0.9,0.9,0.9);
    shrinkTranslation->Translate(-centerTr[0], -centerTr[1], -centerTr[2]);
    vtkSmartPointer<vtkTransformPolyDataFilter> shrinkTransform =  vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    shrinkTransform->SetInputData(transformFilter->GetOutput());
    shrinkTransform->SetTransform(shrinkTranslation);
    shrinkTransform->Update();

    vtkSmartPointer<vtkTransform> translation2 =
            vtkSmartPointer<vtkTransform>::New();
    translation2->Translate(normalTooth);

    vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter2 =
            vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    transformFilter2->SetInputData(shrinkTransform->GetOutput());
    transformFilter2->SetTransform(translation2);
    transformFilter2->Update();

    double* centerTr2 = transformFilter2->GetOutput()->GetCenter();

    vtkSmartPointer<vtkTransform> shrinkTranslation2 =
            vtkSmartPointer<vtkTransform>::New();
    shrinkTranslation2->Translate(centerTr2[0], centerTr2[1], centerTr2[2]);
    shrinkTranslation2->Scale(0.9,0.9,0.9);
    shrinkTranslation2->Translate(-centerTr2[0], -centerTr2[1], -centerTr2[2]);

    vtkSmartPointer<vtkTransformPolyDataFilter> shrinkTransform2 =  vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    shrinkTransform2->SetInputData(transformFilter2->GetOutput());
    shrinkTransform2->SetTransform(shrinkTranslation2);
    shrinkTransform2->Update();

//    pdE->GetPointData()->SetNormals(featureEdges->GetOutput()->GetPointData()->GetNormals());

    double* centerPos = boundaryEdges->GetOutput()->GetCenter();
    double bounds[6];
    boundaryEdges->GetOutput()->GetBounds( bounds );
    double radius = ((bounds[1] - bounds[0]) < (bounds[5] - bounds[4]))? (bounds[1] - bounds[0])/2 : (bounds[5] - bounds[4])/2;
    double h = bounds[3] - bounds[2];

    centerPos[axes]+=10;

    vtkSmartPointer<vtkSphereSource> sphereSource =
            vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetCenter(  centerPos);
    sphereSource->SetRadius(radius/4);
    sphereSource->Update();

    vtkSmartPointer<vtkPlane> plane =
            vtkSmartPointer<vtkPlane>::New();

    plane->SetOrigin(centerPos);
    plane->SetNormal(normalTooth);

    vtkSmartPointer<vtkClipPolyData> clipper =
            vtkSmartPointer<vtkClipPolyData>::New();
    clipper->SetInputConnection(sphereSource->GetOutputPort());
    clipper->SetClipFunction(plane);
    clipper->SetValue(0);
    clipper->Update();

    vtkSmartPointer<vtkPolyDataNormals> normal =
            vtkSmartPointer<vtkPolyDataNormals>::New();
    normal->SetInputConnection(clip->GetOutputPort());
    normal->ConsistencyOn();
    normal->SplittingOff();
    normal->SetFeatureAngle(90);
    normal->Update();

    vtkSmartPointer<vtkRegularPolygonSource> polygonSource3 =
            vtkSmartPointer<vtkRegularPolygonSource>::New();
    polygonSource3->SetNumberOfSides(50);
    polygonSource3->SetRadius(radius/4);
    polygonSource3->SetCenter(centerPos);
    polygonSource3->SetNormal(normalTooth);
    polygonSource3->Update();

    vtkSmartPointer<vtkPolyData> pd3 = vtkSmartPointer<vtkPolyData>::New();
    pd3->SetPolys(polygonSource3->GetOutput()->GetLines());
    pd3->SetPoints(polygonSource3->GetOutput()->GetPoints());
    pd3->SetLines(polygonSource3->GetOutput()->GetLines());

    centerPos[axes]-=2.5;
    // Create a circle
    vtkSmartPointer<vtkRegularPolygonSource> polygonSource1 =
            vtkSmartPointer<vtkRegularPolygonSource>::New();
    polygonSource1->SetNumberOfSides(50);
    polygonSource1->SetRadius(radius/2);
    polygonSource1->SetCenter(centerPos);
    polygonSource1->SetNormal(normalTooth);
    polygonSource1->Update();

    vtkSmartPointer<vtkPolyData> pd1 = vtkSmartPointer<vtkPolyData>::New();
    pd1->SetPolys(polygonSource1->GetOutput()->GetLines());
    pd1->SetPoints(polygonSource1->GetOutput()->GetPoints());
    pd1->SetLines(polygonSource1->GetOutput()->GetLines());

    // Create a circle
    centerPos[axes]-=2.5;

    vtkSmartPointer<vtkRegularPolygonSource> polygonSource2 =
            vtkSmartPointer<vtkRegularPolygonSource>::New();
    polygonSource2->SetNumberOfSides(50);
    polygonSource2->SetRadius((radius/4)+(radius/2));
    polygonSource2->SetCenter(centerPos);
    polygonSource2->SetNormal(normalTooth);
    polygonSource2->Update();

    vtkSmartPointer<vtkPolyData> pd2 = vtkSmartPointer<vtkPolyData>::New();
    pd2->SetPolys(polygonSource2->GetOutput()->GetLines());
    pd2->SetPoints(polygonSource2->GetOutput()->GetPoints());
    pd2->SetLines(polygonSource2->GetOutput()->GetLines());

    // Create a circle
    centerPos[axes]-=2.5;

    // Create a circle
    centerPos[axes]-=0.1;

    vtkSmartPointer<vtkRegularPolygonSource> polygonSource4 =
            vtkSmartPointer<vtkRegularPolygonSource>::New();
    polygonSource4->SetNumberOfSides(50);
    polygonSource4->SetRadius(radius);
    polygonSource4->SetCenter(centerPos);
    polygonSource4->SetNormal(normalTooth);
    polygonSource4->Update();

    vtkSmartPointer<vtkPolyData> pd4 = vtkSmartPointer<vtkPolyData>::New();
    pd4->SetPolys(polygonSource4->GetOutput()->GetLines());
    pd4->SetPoints(polygonSource4->GetOutput()->GetPoints());
    pd4->SetLines(polygonSource4->GetOutput()->GetLines());

    // Combine data sets
    vtkSmartPointer<vtkAppendPolyData> appendFilter = vtkSmartPointer<vtkAppendPolyData>::New();

    appendFilter->AddInputData(pdE.Get());
    appendFilter->AddInputData(shrinkTransform->GetOutput());
    appendFilter->AddInputData(shrinkTransform2->GetOutput());
//    appendFilter->AddInputData(pd2.Get());
//    appendFilter->AddInputData(pd1.Get());
    appendFilter->AddInputData(pd3.Get());
    appendFilter->Update();

    vtkSmartPointer<vtkRuledSurfaceFilter> ruledSurfaceFilter = vtkSmartPointer<vtkRuledSurfaceFilter>::New();
    ruledSurfaceFilter->SetInputData(appendFilter->GetOutput());
    ruledSurfaceFilter->SetRuledModeToResample();
//    ruledSurfaceFilter->SetRuledModeToPointWalk();
    ruledSurfaceFilter->CloseSurfaceOn();
    ruledSurfaceFilter->SetResolution(50, 50);
    ruledSurfaceFilter->Update();

//// smooth???
//    appendFilter->RemoveAllInputs();
//    appendFilter->AddInputData(ruledSurfaceFilter->GetOutput());
//    appendFilter->AddInputData(clipper->GetOutput());
//    appendFilter->Update();
//
//    vtkSmartPointer<vtkSmoothPolyDataFilter> smoothFilter =
//            vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
//    smoothFilter->SetInputConnection(appendFilter->GetOutputPort());
//    smoothFilter->SetNumberOfIterations(15);
//    smoothFilter->SetRelaxationFactor(0.1);
//    smoothFilter->FeatureEdgeSmoothingOff();
//    smoothFilter->BoundarySmoothingOff();
//    smoothFilter->Update();
//
//    // Update normals on newly smoothed polydata
//    vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
//    normalGenerator->SetInputConnection(smoothFilter->GetOutputPort());
//    normalGenerator->ComputePointNormalsOn();
//    normalGenerator->ComputeCellNormalsOn();
//    normalGenerator->Update();
//
//    appendFilter->RemoveAllInputs();
//    appendFilter->AddInputData(normalGenerator->GetOutput());
//    appendFilter->AddInputData(clip->GetOutput());
//    appendFilter->Update();
//
////  end smooth  ----------------------------

    appendFilter->RemoveAllInputs();
    appendFilter->AddInputData(ruledSurfaceFilter->GetOutput());
    appendFilter->AddInputData(clipper->GetOutput());
    appendFilter->AddInputData(clip->GetOutput());
    appendFilter->Update();

    vtkSmartPointer<vtkPolyDataMapper> clipMapper =
            vtkSmartPointer<vtkPolyDataMapper>::New();
    clipMapper->SetInputData(appendFilter->GetOutput());
    clipMapper->ScalarVisibilityOff();

    vtkSmartPointer<vtkActor> clipActor =
            vtkSmartPointer<vtkActor>::New();
    clipActor->SetMapper(clipMapper);
    clipActor->GetProperty()->SetColor(1,1,1) ;

    vtkSmartPointer<vtkPolyDataMapper> clipAllMapper =
            vtkSmartPointer<vtkPolyDataMapper>::New();
    clipAllMapper->SetInputConnection(clipAll->GetOutputPort());
    clipAllMapper->ScalarVisibilityOff();

    actorUp->SetMapper(clipAllMapper);
    vtkSmartPointer<vtkProperty> actorPr = vtkSmartPointer<vtkProperty>::New();
    actorPr->DeepCopy(clipActor->GetProperty());
    actorPr->SetColor(0.6,0.6,0.6);
    actorUp->SetProperty(actorPr);

    renderer->RemoveAllViewProps();
    actorCollection->AddItem(clipActor);
    actorCollection->InitTraversal();
    actorCollection->Modified();
    renderer->AddActor(actorUp);
    renderer->AddActor(actorLo);
    for(vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
    {
        vtkSmartPointer<vtkActor> actor = actorCollection->GetNextActor();
        renderer->AddActor(actor);
    }

    renderWindow->Modified();
    renderWindow->Render();
//    rwi->Start();
}


