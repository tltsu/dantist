/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once


/* INCLUDES *******************************/

#include <QAction>
#include <QMainWindow>
#include <QMenu>
#include <QUndoStack>


/* DECLARATIONS ***************************/

namespace GUI
{
	/**
	 * @brief The MainWindow class provides a template for creating a main
	 * window for applications.
	 *
	 * The MainWindow class takes care about UI routines shared across every
	 * application, like:
	 *
	 * * menus, toolbars and statusbar handling,
	 * * typical menus implementation like File, Edit, View and Help,
	 * * files creation, loading and saving with its complex UI logic.
	 * * toolbars, docking windows (also known as tool windows) and other extra
	 *   elements manipulation and housekeeping,
	 * * UI related settings loading and saving.
	 *
	 * It allows programmers not only abstract of low-level windows assembling
	 * and messages routing as Qt already does, but also get rid of little
	 * but sufficient details of user experience as well and concentrate
	 * on functionality itself.
	 *
	 * To use this class subclass from it and reimplement virtual methods,
	 * defined in protected section.
	 *
	 * Notice: when the window is closed it saves its own UI related settings
	 * as well as for its controls. For the window settings are saved
	 * to "MainWindow" section of a registry or configuration file as specified
	 * by an environment. Controls save their settings into individual
	 * configuration keys. See
	 * [QSettings class documentation](http://doc.qt.io/qt-5/qsettings.html)
	 * for further information about settings storage.
	 */
    class MainWindowTemplate : public QMainWindow
	{
		Q_DISABLE_COPY(MainWindowTemplate)
		Q_OBJECT

	public:
		/**
		 * Constructs a window and loads UI related settings
		 * from "MainWindow" section of a registry or configuration file
		 * as specified by an environment. See
		 * [QSettings class documentation](http://doc.qt.io/qt-5/qsettings.html)
		 * for further information about settings storage.
		 *
		 * After the window is built, it is automatically shown when system
		 * message queue is runned.
		 *
		 * @param initialFilePath A path to a file that will be opened after
		 * the window is shown first time. If empty, new document is created.
		 */
		MainWindowTemplate(QString initialFilePath = QString());

		/**
		 * Returns a reference to Undo/Redo stack of actions. The stack is fully
		 * handled by a main window, so the only duty of a subclass is to add
		 * new actions when necessary.
		 */
		QUndoStack& history();

		/**
		 * Sets what formats may be opened and saved. This information is used
		 * for filtering items in opening and saving dialogs.
		 *
		 * @param filter A list of supported file formats. List items
		 * are splitted with ";;" sequence: @code{.unparsed}
		 * Images (*.png *.xpm *.jpg);;Text files (*.txt);;XML files (*.xml)
		 * @endcode If the parameter is empty, no restriction on operated file
		 * formats is applied.
		 */
		void setSupportedFileFormats(QString filter);

		/** Sets text shown in "About" dialog. HTML formatting is supported. */
		void setAboutText(QString);

	public:
		 virtual /**
		 * Returns a reference to action bound to `Edit->Cut` menu entry.
		 *
		 * This entry is not handled by the window, so custom handler
		 * (e. g. action listener) must be bound to the action.
		 */
		QAction& cutAction();

		virtual QAction& selectAction();

//		virtual void editMenu(QAction *action);

		/**
		 * Returns a reference to action bound to `Edit->Copy` menu entry.
		 *
		 * This entry is not handled by the window, it just has reserved place,
		 * so custom handler (e. g. action listener) must be bound to the action.
		 */
		QAction& copyAction();

		/**
		 * Returns a reference to action bound to `Edit->Paste` menu entry.
		 *
		 * This entry is not handled by the window, it just has reserved place,
		 * so custom handler (e. g. action listener) must be bound to the action.
		 */
		QAction& pasteAction();

		/**
		 * Adds custom actions to `File` menu, just before `Quit` entry.
		 * The added group is splitted from the rest of items by horizontal
		 * lines.
		 */
		void addCustomFileActionsGroup(const QList<QAction*>&);

		/**
		 * Adds custom actions to the end of `Edit` menu.
		 * The added group is splitted from the rest of items by horizontal
		 * lines.
		 */
		void addCustomEditActionsGroup(const QList<QAction*>&);

		/**
		 * Adds custom actions to the end of `View` menu.
		 * The added group is splitted from the rest of items by horizontal
		 * lines.
		 */
		void addCustomViewActionsGroup(const QList<QAction*>&);

		/**
		 * Adds a pane. Panes are dockable windows that allow to operate
		 * a document presented in a main window, being optional. A user may
		 * choose, whether to bind the pane to some side or stack it
		 * with other ones, keep it floating, close it or show again
		 * via `View->Docks`.
		 *
		 * This method makes these steps:
		 *
		 * * Adds the *pane* to the MainWindow for which this method is called for.
		 * * Loads pane specific configuration if presented. The configuration
		 *   is loaded from "%pane.objectName()%" section of a registry
		 *   or configuration file. See
		 *   [QSettings class documentation](http://doc.qt.io/qt-5/qsettings.html)
		 *   for further information about settings storage.
		 * * Adds a command in `View->Panes` to control a presence of the pane
		 *   on a screen.
		 * * Shows the pane.
		 *
		 * Notice: the dock must have its objectName() set to non-empty string.
		 * This property is used to distinguish panes in the configuration.
		 *
		 * @param defaultArea an area where the pane is bound to
		 * if no configuration is provided.
		 */
		void addPane(QDockWidget& pane, Qt::DockWidgetArea defaultArea);

	public slots:
		/**
		 * Triggers `File->New` menu entry like it was clicked by user.
		 *
		 * Before creating a document user is prompted to save changes if any
		 * presented. If user accepts, save() method is invoked,
		 * otherwise document creation is cancelled.
		 *
		 * For document creation createDocument() is invoked.
		 */
		void create();

		/**
		 * Triggers `File->Open` menu entry like it was clicked by user.
		 *
		 * "Open" dialog allows to choose only files with formats formats
		 * specified by setSupportedFileFormats().
		 *
		 * Before showing document opening dialog user is prompted to save
		 * changes if any presented. If user accepts, save() method is invoked,
		 * otherwise document opening is cancelled.
		 *
		 * When file is chosen openDocument() is invoked.
		 */
		void open();

		/**
		 * Triggers `File->Save` menu entry like it was clicked by user.
		 *
		 * If a document is saved for the first time, saveAs() is invoked
		 * to gather a path to a file.
		 *
		 * If user cancels "Save as" dialog, this method returns `false`.
		 * In this case caller must abort any operation that follows
		 * after saving and restore document state that was before performing
		 * the operation.
		 *
		 * When file is successfully determined saveDocument() is invoked.
		 *
		 * @returns if a caller may perform some extra actions. `False`
		 * is returned if at least one condition is met:
		 * * user answered Cancel to "Save before exit?" dialog,
		 * * user closed "Save As" dialog by close button or Cancel button,
		 * * saving failed in some way.
		 */
		bool save();

		/**
		 * Triggers `File->Save As...` menu entry like it was clicked by user.
		 *
		 * Shows "Save" dialog where user can specify where to save a document
		 * and specify format (e. g. extension) from ones listed by last
		 * setSupportedFileFormats() invocation.
		 *
		 * This function behaves the same as save(). The only difference is that
		 * it forcely shows files dialog.
		 *
		 * @returns if a caller may perform some extra actions. `False`
		 * is returned if at least one condition is met:
		 * * user answered Cancel to "Save before exit?" dialog,
		 * * user closed "Save As" dialog by close button or Cancel button,
		 * * saving failed in some way.
		 */
		bool saveAs();

		/**
		 * Triggers `Help->About...` menu entry like it was clicked by user.
		 *
		 * The shown "About" dialog contains application icon from a main window
		 * and text specified by setAboutText() function before.
		 */
		void showAbout();

		void cut();
		void select();
//		void edit(QAction* action);

	protected:
		/**
		 * Returns a full path to currently opened document. If a document
		 * was not saved before so it has no location, empty string is returned.
		 */
		virtual QString documentPath() const = 0;

		/**
		 * A custom routine that is called for creating new document, e. g.
		 * clear all document related stuff like an application was just runned.
		 * When this function is invoked all checks are already passed,
		 * saveDocument() is successfully called if necessary and main window
		 * state is cleaned.
		 *
		 * After this function execution documentPath() must return an empty
		 * string.
		 *
		 * New document is assumed to be the safest one so document creation
		 * failures are unhandled.
		 *
		 * Important notice: this function is NOT called when main window
		 * is created, only when something explicitly invoked create().
		 */
		virtual void createDocument() = 0;

		/**
		 * A custom routine that is called for opening a document, e. g. loading
		 * its content from a disc. When this function is invoked all checks
		 * are already passed and saveDocument() is successfully called if
		 * necessary.
		 *
		 * This function must handle entities that are created by a subclass;
		 * window related routines (like undo stack) are performed automatically
		 * out of a scope of this function.
		 *
		 * After successful execution of this function (e. g. if it returned
		 * `true`) documentPath() must return a string passed as *path*
		 * parameter.
		 *
		 * If document loading fails, every change made by this function must
		 * be undone like the function was not called. To achive it the file
		 * should be extracted not directly into currectly opened document, but
		 * into some temporary place with following swapping the current and
		 * the new ones. This guarantees that if exception occurs either the old
		 * document will be restored (if it failed before swapping) or the new
		 * one will be fully applied (if it failed after swapping).
		 *
		 * @param path a path to a file that is needed to be opened.
		 * @return if file opening was performed successfully.
		 */
		virtual bool openDocument(QStringList path) = 0;

		/**
		 * A custom routine that is called for saving current document. When
		 * this function is invoked all checks are already passed and file path
		 * to save is already retrieved so it is safe to just write file content.
		 *
		 * If document saving fails, original file must be restored like
		 * the function was not called. To achive it the file
		 * should be written to temporary file first. It may be achieved
		 * by using [QSaveFile](http://doc.qt.io/qt-5/qsavefile.html) that does
		 * the same but in transparent way.
		 *
		 * @param path to new save destination if not empty. If empty,
		 * an existing path from documentPath() is taken.
		 *
		 * @return if file saving was performed successfully.
		 */
		virtual bool saveDocument(QString path = QString()) = 0;

		/**
		 * Prompts user to save file and after successful saving via save()
		 * closes a window. If user aborts saving closing is cancelled.
		 *
		 * Cleanup and configuration saving is performed by destructor of this
		 * class after the window is closed.
		 *
		 * See [QWidget::closeEvent() documentation](http://doc.qt.io/qt-4.8/qwidget.html#closeEvent)
		 * for further details about closing event handler.
		 */
		void closeEvent(QCloseEvent*) override;

	private slots:
		void cleanChanged(bool isClean);

	private:
		static QString serializeArea(Qt::DockWidgetArea);
		static Qt::DockWidgetArea deserializeArea(QString);


		void loadSettings();
		void saveSettings();

		void initActions();
		void initMenus();

		void openByPath(QStringList);
		bool checkDocumentSaving();

		QString _supportedFileFormats;
		QString _aboutText;

		QUndoStack _history;

		// GUI

		QMenu    _file;
		QAction    _fileNew;
		QAction    _fileOpen;
		QAction    _fileSave;
		QAction    _fileSaveAs;
		QAction    _fileQuit;
		QMenu    _edit;
		QAction*   _editUndo;
		QAction*   _editRedo;
		QAction    _editCut;
		QAction    _editSelect;
		QAction    _editCopy;
		QAction    _editPaste;
		QMenu    _view;
		QMenu      _viewPanes;
		QMenu    _help;
		QAction    _helpAbout;
		QAction    _helpAboutQt;


	};
}
