/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* INCLUDES *******************************/

#include <QApplication>
#include <QDockWidget>
#include <QMenuBar>
#include <QSettings>
#include "main_window_template.hpp"


/* PUBLIC FUNCTIONS **********************/

// "About" menu captions are not followed by an ellipsis because Raymond Chen
// settled that in his "When do you put ... after a button or menu?" article:
// http://blogs.msdn.com/b/oldnewthing/archive/2004/05/17/133181.aspx

GUI::MainWindowTemplate::MainWindowTemplate(QString initialFilePath)
    : _file(tr("&File"), menuBar())
	, _fileNew(tr("&New"), &_file)
	, _fileOpen(tr("&Open"), &_file)
	, _fileSave(tr("&Save"), &_file)
	, _fileSaveAs(tr("Save &As..."), &_file)
	, _fileQuit(tr("&Quit"), &_file)
	, _edit(tr("&Edit"), menuBar())
	, _editUndo(_history.createUndoAction(&_edit))
	, _editRedo(_history.createRedoAction(&_edit))
	, _editSelect(tr("&Select"), &_edit)
	, _editCut(tr("&Cut"), &_edit)
	, _editCopy(tr("C&opy"), &_edit)
	, _editPaste(tr("&Paste"), &_edit)
	, _view(tr("&View"), menuBar())
	, _viewPanes(tr("&Panes"), &_view)
	, _help(tr("&Help"), menuBar())
	, _helpAbout(tr("&About %1").arg(QCoreApplication::applicationName()), &_help)
	, _helpAboutQt(tr("About &Qt"), &_help)

{
	initActions();
	initMenus();

	connect(&_history, &QUndoStack::cleanChanged, this, &MainWindowTemplate::cleanChanged);
	connect(&_history, &QUndoStack::cleanChanged, &_fileSave, &QAction::setDisabled);

	loadSettings();
	setWindowFilePath(tr("[Untitled]"));

	if(!initialFilePath.isEmpty()) {
		QStringList list = QStringList();
		list.append(initialFilePath);
		openByPath(list);
	}

}


/* PRIVATE FUNCTIONS **********************/

void GUI::MainWindowTemplate::loadSettings()
{
	QSettings settings;
	settings.beginGroup(QStringLiteral("MainWindow"));

	const QVariant pos = settings.value(QStringLiteral("Position"));
	const QPoint position = pos.value<QPoint>();
	if(!position.isNull())
		move(position);

	const QVariant size = settings.value(QStringLiteral("Size"));
	const QSize windowSize = size.value<QSize>();
	if(!windowSize.isEmpty())
		resize(windowSize);

	const QVariant max = settings.value(QStringLiteral("IsMaximized"));
	const bool isMaximized = max.value<bool>();
	if(isMaximized)
		showMaximized();
	else
		show();
}

void GUI::MainWindowTemplate::saveSettings()
{
	QSettings settings;

	settings.beginGroup(QStringLiteral("MainWindow"));

	settings.setValue(QStringLiteral("Geometry"), normalGeometry());
	settings.setValue(QStringLiteral("Size"), size());
	settings.setValue(QStringLiteral("IsMaximized"), isMaximized());

	QObject* child;
	for(auto it = children().begin(), end = children().end(); it != end; ++it)
	{
		child = *it;

		QDockWidget* const dock = qobject_cast<QDockWidget*>(child);
		if(dock)
		{
			settings.beginGroup(dock->objectName());

			settings.setValue(QStringLiteral("Position"), dock->pos());
			settings.setValue(QStringLiteral("Size"), dock->size());

			const bool isVisible = dock->toggleViewAction()->isChecked();
			settings.setValue(QStringLiteral("Visibility"), isVisible);

			const QString areaId = serializeArea(dockWidgetArea(dock));
			settings.setValue(QStringLiteral("DockingArea"), areaId);

			settings.endGroup();
		}
	}

	settings.endGroup();
}

void GUI::MainWindowTemplate::initActions()
{
	_fileNew.setShortcut(QKeySequence::New);
	connect(&_fileNew, &QAction::triggered, this, &MainWindowTemplate::create);

	_fileOpen.setShortcut(QKeySequence::Open);
	connect(&_fileOpen, &QAction::triggered, this, &MainWindowTemplate::open);

	_fileSave.setShortcut(QKeySequence::Save);
	_fileSave.setEnabled(false);
	connect(&_fileSave, &QAction::triggered, this, &MainWindowTemplate::save);

	_fileSaveAs.setShortcut(QKeySequence::SaveAs);
	connect(&_fileSaveAs, &QAction::triggered, this, &MainWindowTemplate::saveAs);

	_fileQuit.setMenuRole(QAction::QuitRole);
	_fileQuit.setShortcut(QKeySequence::Quit);
	connect(&_fileQuit, &QAction::triggered, this, &QWidget::close);

	_editUndo->setShortcut(QKeySequence::Undo);
	_editRedo->setShortcut(QKeySequence::Redo);

	_editSelect.setShortcut(QKeySequence::SelectAll);
	_editSelect.setEnabled(true);
	connect(&_editSelect, &QAction::triggered, this, &MainWindowTemplate::select);

	_editCut.setShortcut(QKeySequence::Cut);
	_editCut.setEnabled(true);
	connect(&_editCut, &QAction::triggered, this, &MainWindowTemplate::cut);



	_editCopy.setShortcut(QKeySequence::Copy);
	_editCopy.setEnabled(false);

	_editPaste.setShortcut(QKeySequence::Paste);
	_editPaste.setEnabled(false);

	_helpAbout.setMenuRole(QAction::AboutRole);
	connect(&_helpAbout, &QAction::triggered, this, &MainWindowTemplate::showAbout);

	_helpAboutQt.setMenuRole(QAction::AboutQtRole);
	connect(&_helpAboutQt, &QAction::triggered, this, &QApplication::aboutQt);
}

void GUI::MainWindowTemplate::initMenus()
{
	menuBar()->addMenu(&_file);
	_file.addAction(&_fileNew);
	_file.addAction(&_fileOpen);
	_file.addAction(&_fileSave);
	_file.addAction(&_fileSaveAs);
	_file.addSeparator();
	_file.addAction(&_fileQuit);


	menuBar()->addMenu(&_edit);
	_edit.addAction(_editUndo);
	_edit.addAction(_editRedo);
	_edit.addSeparator();
	_edit.addAction(&_editSelect);
	_edit.addAction(&_editCut);
	_edit.addAction(&_editCopy);
	_edit.addAction(&_editPaste);

	menuBar()->addMenu(&_view);
	_view.addMenu(&_viewPanes);

	menuBar()->addMenu(&_help);
	_help.addAction(&_helpAbout);
	_help.addAction(&_helpAboutQt);


}
