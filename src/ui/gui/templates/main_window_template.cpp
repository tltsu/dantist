/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* INCLUDES *******************************/

#include <QCloseEvent>
#include <QCoreApplication>
#include <QDockWidget>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QSettings>
#include "main_window_template.hpp"


/* PUBLIC FUNCTIONS **********************/

QUndoStack& GUI::MainWindowTemplate::history()
{
	return _history;
}

void GUI::MainWindowTemplate::setSupportedFileFormats(QString filter)
{
	_supportedFileFormats = filter;
}

void GUI::MainWindowTemplate::setAboutText(QString text)
{
	_aboutText = text;
}

QAction& GUI::MainWindowTemplate::cutAction()
{
	return _editCut;
}

QAction& GUI::MainWindowTemplate::selectAction()
{
	return _editSelect;
}

//
//void GUI::MainWindowTemplate::editMenu(QAction* action)
//{
////	return _editSelect;
//}


QAction& GUI::MainWindowTemplate::copyAction()
{
	return _editCopy;
}

QAction& GUI::MainWindowTemplate::pasteAction()
{
	return _editPaste;
}

void GUI::MainWindowTemplate::addCustomFileActionsGroup(const QList<QAction*>& list)
{
	_file.insertActions(&_fileQuit, list);
	_file.insertSeparator(&_fileQuit);
}

void GUI::MainWindowTemplate::addCustomEditActionsGroup(const QList<QAction*>& list)
{
	_edit.addSeparator();
	_edit.addActions(list);
}

void GUI::MainWindowTemplate::addCustomViewActionsGroup(const QList<QAction*>& list)
{
	QAction* const panesMenuAction = _viewPanes.menuAction();

	_view.insertActions(panesMenuAction, list);
	_view.insertSeparator(panesMenuAction);
}

void GUI::MainWindowTemplate::addPane(QDockWidget& pane, Qt::DockWidgetArea defaultArea)
{
	QSettings dockSettings;
	dockSettings.beginGroup(pane.objectName());

	const QVariant pos = dockSettings.value(QStringLiteral("Position"));
	const QPoint position = pos.value<QPoint>();
	pane.move(position);

	const QVariant a = dockSettings.value(QStringLiteral("DockingArea"));
	if(a.isValid())
	{
		const Qt::DockWidgetArea area = deserializeArea(a.value<QString>());
		addDockWidget(area, &pane);
	}
	else
		addDockWidget(defaultArea, &pane);

	const QVariant size = dockSettings.value(QStringLiteral("Size"));
	const QSize dockSize = size.value<QSize>();
	pane.resize(dockSize);

	const QVariant vis = dockSettings.value(QStringLiteral("Visibility"), true);
	const bool isVisible = vis.value<bool>();
	pane.toggleViewAction()->setChecked(isVisible);

	_viewPanes.addAction(pane.toggleViewAction());
}


/* SLOTS **********************************/

void GUI::MainWindowTemplate::create()
{
	if(checkDocumentSaving())
	{
		createDocument();
		setWindowFilePath(tr("[Untitled]"));

		// The history must be cleared after any other action is done. It must
		// be so because spurious actions may pe put into the history while
		// document creation.
		_history.clear();
	}
}

void GUI::MainWindowTemplate::open()
{
	if(checkDocumentSaving())
	{
		const QStringList path = QFileDialog::getOpenFileNames
			(this, QString(), QString(), _supportedFileFormats);

		if(!path.empty()) {
			openByPath(path);
		}


	}
}

bool GUI::MainWindowTemplate::save()
{
	if(!documentPath().isNull())
	{
		if(saveDocument())
		{
			_history.setClean();
			return true;
		}
		else
			return false;
	}
	else
		return saveAs();
}


void GUI::MainWindowTemplate::cut()
{
	this->cutAction();
}


void GUI::MainWindowTemplate::select()
{
	this->selectAction();
}
//
//void GUI::MainWindowTemplate::edit( QAction* action)
//{
// this->editMenu(action);
//}

bool GUI::MainWindowTemplate::saveAs()
{
	const QString path = QFileDialog::getSaveFileName
		(this, QString(), QString(), _supportedFileFormats);

	if(!path.isNull() && saveDocument(path))
	{
		setWindowFilePath(QFileInfo(path).fileName());
		_history.setClean();
		return true;
	}
	else
		return false;
}

void GUI::MainWindowTemplate::showAbout()
{
	const QString aboutTitle = tr("About %1")
		.arg(QCoreApplication::applicationName());

	QMessageBox::about(this, aboutTitle, _aboutText);
}

void GUI::MainWindowTemplate::cleanChanged(bool isClean)
{
	setWindowModified(!isClean);
}


/* PRIVATE FUNCTIONS **********************/

void GUI::MainWindowTemplate::closeEvent(QCloseEvent* event)
{
	if(checkDocumentSaving())
		saveSettings();
	else
		event->ignore();
}



QString GUI::MainWindowTemplate::serializeArea(Qt::DockWidgetArea area)
{
	switch(area)
	{
		case Qt::TopDockWidgetArea:
			return QStringLiteral("Top");
		case Qt::BottomDockWidgetArea:
			return QStringLiteral("Bottom");
		case Qt::LeftDockWidgetArea:
			return QStringLiteral("Left");
		case Qt::RightDockWidgetArea:
			return QStringLiteral("Right");
		default:
			return QString();
	}
}

Qt::DockWidgetArea GUI::MainWindowTemplate::deserializeArea(QString raw)
{
	if(raw == QStringLiteral("Top"))
		return Qt::TopDockWidgetArea;
	else if(raw == QStringLiteral("Bottom"))
		return Qt::BottomDockWidgetArea;
	else if(raw == QStringLiteral("Left"))
		return Qt::LeftDockWidgetArea;
	else if(raw == QStringLiteral("Right"))
		return Qt::RightDockWidgetArea;
	else
		return Qt::NoDockWidgetArea;
}

void GUI::MainWindowTemplate::openByPath(QStringList path)
{
	if(openDocument(path))
	{
//		setWindowFilePath(QFileInfo(path).fileName());

		// The history must be cleared after any other action is done. It must
		// be so because spurious actions may be put into the history while
		// document opening.
		_history.clear();
	}
}

bool GUI::MainWindowTemplate::checkDocumentSaving()
{
	if(!_history.isClean())
	{
		const int answer = QMessageBox::question
		(
			this,
			tr("Saving confirmation"),
			tr("Save changes to %1 before closing?").arg(windowFilePath()),
			QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
			QMessageBox::Cancel
		);

		return (answer == QMessageBox::Yes)
			? save() : (answer == QMessageBox::No);
	}
	else
		return true;
}
