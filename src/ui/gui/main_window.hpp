/*
 * Copyright 2015 Oleg Yarigin <arhad95@gmail.com>.
 *
 * This file is part of WolfDK.
 *
 * WolfDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WolfDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WolfDK. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#pragma once


/* INCLUDES *******************************/

#include <QGroupBox>
#include <QLabel>
#include <QSlider>
#include <QVTKWidget.h>
#include <core/document.hpp>
#include "ui/gui/templates/main_window_template.hpp"


/* DECLARATIONS ***************************/

namespace GUI
{
    class MainWindow : public MainWindowTemplate
    {
        Q_DISABLE_COPY(MainWindow)
        Q_OBJECT

    public:
        MainWindow(QString openedFilePath = QString());

    protected:
        QString documentPath() const override;

        void createDocument() override;
        bool openDocument(QStringList path) override;
        bool saveDocument(QString path) override;

    private slots:
        void axisRotationChanged();
        void useAxis(bool);

    private:
        void initComponents();
        void initLayout();

        Document _document;

    public:
        QAction &cutAction() override;
        QAction &selectAction() override;

        QVTKWidget _viewport;
        QGroupBox    _axisRotation;
        QLabel         _axisXRotationLabel;
        QSlider        _axisXRotation;
        QLabel         _axisYRotationLabel;
        QSlider        _axisYRotation;
        QLabel         _axisZRotationLabel;
        QSlider        _axisZRotation;
        QWidget    _mainWindowContent;



    public slots:

        void editMenu (QAction* action);

        void popup(vtkObject * obj, unsigned long, void * client_data,
                   void *, vtkCommand * command);
    };
}
