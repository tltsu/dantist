cmake_minimum_required(VERSION 2.8.11)

if(POLICY CMP0043)
  cmake_policy(SET CMP0043 NEW)
endif()

if(POLICY CMP0020)
	cmake_policy(SET CMP0020 NEW)
endif()

find_package(Qt5Widgets REQUIRED QUIET)
find_package(VTK REQUIRED)

include_directories(app
                    ${VTK_USE_FILE}
					Resources
					.
)
include(${VTK_USE_FILE})

set(CMAKE_AUTOMOC ON)


if (APPLE)
	find_library( LIB_COCOA cocoa )
	set(source
			core/document.cpp
			core/init/commandline_options.cpp
			ui/gui/main_window.cpp
			ui/gui/templates/main_window_template.cpp
			ui/gui/templates/main_window_init_tpl.cpp
			main.cpp
			ui/gui/KeyPressInteractorStyle.h
			ui/gui/KeyPressInteractorStyle.cpp
			ui/gui/osxHelper.mm
			ui/gui/osxHelper.h)
	add_executable(dantist MACOSX_BUNDLE  ${source})

	else()
	set(source
			core/document.cpp
			core/init/commandline_options.cpp
			ui/gui/main_window.cpp
			ui/gui/KeyPressInteractorStyle.h
			ui/gui/KeyPressInteractorStyle.cpp
			ui/gui/templates/main_window_template.cpp
			ui/gui/templates/main_window_init_tpl.cpp
			main.cpp
			)
	add_executable(dantist ${source})
endif ()

qt5_use_modules(dantist Core Gui)

set_target_properties(dantist
	PROPERTIES
	AUTOMOC TRUE
	ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../lib"
	LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../lib"
	RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../bin"
)

target_compile_features(dantist
	PRIVATE
	"cxx_auto_type"
	"cxx_lambdas"
	"cxx_override"
	"cxx_nullptr"
	"cxx_right_angle_brackets"
	"cxx_rvalue_references"
)

if(VTK_LIBRARIES)
  target_link_libraries(dantist ${VTK_LIBRARIES})
else()
  target_link_libraries(dantist vtkHybrid vtkWidgets)
endif()

if (APPLE)

	configure_file(
		${CMAKE_CURRENT_SOURCE_DIR}/Resources/Info.plist
		${CMAKE_CURRENT_BINARY_DIR}/Info.plist
	)

	set_target_properties(dantist PROPERTIES MACOSX_BUNDLE_INFO_PLIST
			${CMAKE_CURRENT_BINARY_DIR}/Info.plist)

endif ()

